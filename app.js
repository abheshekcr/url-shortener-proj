var express = require('express'),
mongoose = require('mongoose'),
passport = require('passport'),
User =require('./models/user'),
UserUrl =require('./models/url'),
bodyParser = require('body-parser'),
LocalStratergy = require('passport-local'),
passportLocalMongoose = require('passport-local-mongoose');

mongoose.connect("mongodb://localhost/shortener_app");

var app =express();
app.use(bodyParser.urlencoded({extended:true}));

app.use(require('express-session')({
    secret:"abc",
    resave:false,
    saveUninitialized:false

}));



app.set('view engine' ,'ejs');
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStratergy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.get('/',function(req,res){
    res.render('home');
});

app.get('/register',function(req,res){
    res.render('register');
});

app.get('/login',function(req,res){
    res.render('login');
});

app.get('/enter',function(req,res){
    res.render('enter');
});

app.post('/register',function(req,res){
    
    User.register(new User({username:req.body.username}),req.body.password,function(err,data){
        if(err){
            console.log(err);
            return res.render('register');
        }else{
            passport.authenticate("local")(req,res,function(){
                res.redirect('enter');
            });
        }
    });
});

app.post('/enter',function(req,res){

    UserUrl.create({
        givenUrl:req.body.message,
        genUrl:req.body.sample,
        date:new Date()
    },function(err,data){
        if(err){
            console.log(err);
        }else{
            console.log(data);
            res.redirect('/');
        }
    });

});

  /*  UserUrl.create(req.body.message, function(err,data){
        if(err){
            console.log(err);
        } else {
            data.givenUrl = req.body.message;
           data.save();
            console.log(data);
            res.redirect('/');
        }
  });
  */



 
app.post('/login',passport.authenticate("local",{
    successRedirect:"/enter",
    failureRedirect:"/login"
}),function(req,res){

});

app.get('/logout',function(req,res){
    req.logout();
    res.redirect('/');
});


//get url from user 
//shorten it to 5 digit alphanumeric link
//display it at the bottom..
//long-tshort url is stored in database along wih creation time 
//when generated url matches with one in db it throws error..
//wen used that link it redirects to the original page

app.listen(3000,function(req,res){
    console.log('server started');
});



