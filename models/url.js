var mongoose = require('mongoose');
var passportLocalMongoose = require('passport-local-mongoose');
var urlSchema = new mongoose.Schema({
    givenUrl: String,
    genUrl:String,
    date: { type: Date, default: Date.now },

  });

urlSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model("UserUrl",urlSchema);